class CreateFbusers < ActiveRecord::Migration
  def self.up
    create_table :fbusers do |t|
      t.string :uid
      t.string :name
      t.date   :birthday
      t.text   :status
      t.string :relation
      t.string :gender
      t.string :first_name
      t.string :last_name
      t.string :hometown_location
      t.string :networking
      t.string :current_location      
      t.boolean:deleted, :default => false
      t.boolean:invited_enough, :default => false
      t.string :affiliations
      t.integer :friends_invited, :default => 0
      t.timestamps
    end

  end

  def self.down
    drop_table :fbusers
  end
end
