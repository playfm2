# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_justusers_session',
  :secret      => 'bb10f90ec1bc617c176d8590170f5da27ecb60dcda18f03a7c417a88dcdeeb62e98af76738b59a3ebddadff26b7fdb80fd384ae81d3c03bafe5f16fb6b8ab136'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
