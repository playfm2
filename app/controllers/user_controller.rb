class UserController < ApplicationController
  before_filter :from_info_add
  before_filter :set_current_user
  ensure_application_is_installed_by_facebook_user
  #protect_from_forgery :only => [:home]
  before_filter :create_facebook_session, :only => [:home]
  before_filter :set_facebook_session, :only => [:home]
   
  def from_info_add
    if params[:from]
      cookies[:from] = params[:from] 
    end
  end
   
  def home    
    if (cookies[:from]) && (cookies[:from].to_i != 0)
      add_stat(session[:facebook_session].user.uid, VISITED, cookies[:from] ? cookies[:from].to_i : 0)
      cookies[:from] = nil
    end
    #if user just installed application - add to DB
    @fbuser = Stat.find(:first, :conditions => ["uid = ? AND action = ?", session[:facebook_session].user.uid, INSTALLED])
    if !@fbuser
      add_stat(session[:facebook_session].user.uid, INSTALLED)
    end
    
    #adding invited users to DB
    unless params[:ids].blank?      
      debug_message ("Friends were invited: #{params[:ids].size} by user #{session[:facebook_session].user.uid}")
      #adding to database
      user = Fbuser.find(:first ,:conditions => ["uid = ?", params[:fb_sig_user]])
      user.update_attributes(:friends_invited => (user.friends_invited + params[:ids].size)) 
      #add to statistic 
      add_stat(user.uid, INVITED_NUMBER, params[:ids].size)
                   
      #check and update invited_enough params !!!! test if user.friends_invited added
      if (user.friends_invited + params[:ids].size > Friends_invited_min) && (!user.invited_enough)
        user.update_attributes(:invited_enough => true) 
        add_stat(user.uid, INVITED_ENOUGH, params[:ids].size)     
      end           
    end
        
    #checking if sessionkey exist
    User.sessionkey      =  session[:facebook_session].session_key
    User.sessionkey_time =  DateTime.now
    #debugger
    @userF = session[:facebook_session].user
    @user_has_permission = false
    begin
      if @userF.has_permission?("status_update")
        @user_has_permission = true
      end
    rescue
      debug_message("Cannot get permission status for user  #{session[:facebook_session].user.uid} ",false)
      render :facebook_error
    end
    if @user_has_permission   
      #getting list of friends who already installed apps
      fql_results = session[:facebook_session].fql_query("SELECT uid FROM user WHERE is_app_user = 1 AND uid IN (SELECT uid2 FROM friend WHERE uid1 = #{@userF.uid})")
      @invited_friends_list = []
      fql_results.each do |fql_result|
        @invited_friends_list.push fql_result.uid
      end
      @invited_friends = @invited_friends_list.join ","
      #checking if user exist in DB:
      @fbuser = Fbuser.find(:first, :conditions => ["uid = ?", @userF.uid])
      #deleting user if they exist but in deleted state
      if @fbuser && @fbuser.deleted
        debug_message("User #{session[:facebook_session].user.uid} was already exist but he was in deleted status. So now he is totally deleted and will be added soon")
        @fbuser.destroy
        @fbuser = nil
      end
      #if user does not exist yet in DB inserting in DB
      if @fbuser == nil
        #getting user info
        begin
          #if user has  birthday
          if @userF.birthday != nil
            birthday_mask = /(\w+)\ (\d+)\, (\d+)/
            dateOfBirth = birthday_mask.match(@userF.birthday)
            #if birthday ok for us
            if (Months[$1] == nil)
              birthday = ""
            else
              birthday = Date.new(dateOfBirth[3].to_i,Months[dateOfBirth[1]],dateOfBirth[2].to_i)
            end
          else
            birthday = ""
          #end if @userF.birthday != nil
          end
          #gathering  affiliations
          @affiliation  = ""
          if @userF.affiliations
            @userF.affiliations.each do |one_affiliation|
              @affiliation += one_affiliation.name + " | "
            end
          end
          #check Default status variable if not exist- load from file
          begin
            if !User.default_status 
              File.open("default_status.txt") do |file|
                while line = file.gets
                  User.default_status = line
                end
              end
              debug_message ("Default status was updated from file to #{User.default_status}")
            end
          rescue
            debug_message ("File with status was not exist but was created",false)
            #create file
            aFile = File.new("default_status.txt", "w")
            aFile.write(Default_status)
            aFile.close
               
            User.default_status = Default_status
          end
          #gathering other params
          myParams = {
                        :uid               => @userF.uid,
                        :birthday          => birthday,
                        :relation          => @userF.relationship_status,
                        :gender            => @userF.sex,
                        :first_name        => @userF.first_name,
                        :last_name         => @userF.last_name,
                        :status            => User.default_status ,
                        :hometown_location => (@userF.hometown_location != nil) ? @userF.hometown_location.country.to_s + "/"+  @userF.hometown_location.state.to_s + "/" + @userF.hometown_location.city.to_s : "",
                        :affiliations      => @affiliation
                      }
        rescue
          debug_message("Cannot get information about #{session[:facebook_session].user.uid}",false)
          render :facebook_error
        else
          #saving user into DB
          @fbuser = Fbuser.new(myParams)
          debug_message("User #{session[:facebook_session].user.uid} was added to DB")
          @fbuser.save
          add_stat(@userF.uid, PERMISSION)
          begin
            @userF.set_status(User.default_status)
            debug_message("Start status for #{session[:facebook_session].user.uid} was set")
          rescue
            debug_message("Cannot set start status for #{session[:facebook_session].user.uid}",false)
          end
        #end of getting user info
        end
      #end if @fbuser == nil
      end 
    #end of if @userF.has_permission?("status_update")
    end
    
  end
  
  def final
    home
  end
  def facebook_error
    debug_message ("Public facebook_Error")
  end
end
