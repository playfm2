require 'csv'
class FbusersController < ApplicationController
  #before_filter :set_current_user, :only => [:status_change, :notification_send]
  #skip_before_filter :verify_authenticity_token 
  #before_filter :create_facebook_session, :only => [:status_change, :notification_send]
  #before_filter :set_facebook_session, :only => [:status_change, :notification_send]

  #preparing querry (gender + relation + age + deleted?) for further work
  
  def convert_birthday_to_number(db_birthday)
    ((DateTime.now - db_birthday)/365).to_i
  end
 
  def export_table_to_csv
    query_prepary(params[:query_type].to_i) 
    
    columns = "first_name,last_name,created_at,birthday,gender,relation,hometown_location,networking,deleted,uid,current_location,affiliations,friends_invited,status"
    @results = Fbuser.find(:all,:select => columns, :conditions => [@query_string ,@query_params],:order => "created_at")
    @titles  = "First name,Last name,Permission date,Birthday,Gender,Relation,Hometown location,Networks,If deleted,Facebook id,Current location,Affiliations,Friends invited,Status,Age".split(",")
    report = StringIO.new
    CSV::Writer.generate(report, ',') do |title|
      title << @titles
      @titles  = columns.split(",")
      @results.each do |result|
        
        temp = @titles.map { |a| result.send(a) }
        age  = result[:birthday] = result[:birthday] ? convert_birthday_to_number(result[:birthday]) : nil  
        temp = temp + [age]
        title << temp
        
      end
    end   
    report.rewind

    time_now = Time.now.to_formatted_s(:number)
    file = File.open("#{RAILS_ROOT}/public/#{time_now}.csv", "wb")
    file.write(report.read)
    file.close
    
    redirect_to "/#{time_now}.csv"

  end
  
  def query_prepary (query_type) 
    #debugger
    @query_string = "deleted = :deleted"
    @query_params = Hash.new(0)  
    
    #@query_params[:friends_invited] = Friends_invited_min
    case (query_type)    
      when Deleted
        @query_params[:deleted] = Deleted
        #@query_string = @query_string + " AND friends_invited >= :friends_invited"
      when UnDeleted
        @query_params[:deleted] = UnDeleted
        #@query_params[:invited_enough] = true
        #@query_string = @query_string + " AND invited_enough = :invited_enough"
      when UnInvited
        @query_params[:deleted] = UnDeleted
       # @query_params[:invited_enough] = false
        #@query_string = @query_string + " AND invited_enough = :invited_enough"
    end
    
    #gender querry
    if params[:gender] && params[:gender] != "0"    
      if params[:gender] == "Undefined"
        @query_params[:gender] = ""
      else
        @query_params[:gender] = params[:gender]
      end   
      @query_string = @query_string + " AND gender = :gender"
    end
    #relation querry
    if params[:relation] && params[:relation] != "0"    
      if params[:relation] == "Undefined"
        @query_params[:relation] = ""
      else
        @query_params[:relation] = params[:relation]  
      end
      @query_string = @query_string + " AND relation = :relation"
    end
    #age querry
    if params[:age] && params[:age] != "0"
      if params[:age] == "Undefined"
        @query_string = @query_string + " AND birthday is NULL"
      else
        if params[:age] != "35"
          birthday_start  = Time.now - (params[:age].to_i + 1) * 1.year   
          birthday_finish = Time.now - (params[:age].to_i ) * (1.year) 
        else
          birthday_start  = Time.now - 100.year
          birthday_finish = Time.now - 35.year
        end 
        @query_params[:birthday_start]  = birthday_start
        @query_params[:birthday_finish] = birthday_finish
        @query_string = @query_string + " AND birthday >= :birthday_start  AND birthday <= :birthday_finish"
      end     
    end
  #end def query_prepary (deleted)
  end 
  #authification
  def auth
    if (params[:login])
      if (params[:login] == FBlogin) and (params[:password] == FBpassword)
        cookies[:auth] = "ok" 
        flash[:notice] = 'Successfully logged in'                  
        respond_to do |format|
          format.html { redirect_to(fbusers_url) }
        end        
      else
        flash[:notice] = 'Incorrect login and/or password'
      end
    end    
  #def auth 
  end
  def sign_out
    cookies[:auth] = "false" 
    respond_to do |format|
      format.html { render :action => "auth" }
    end
  end
  #main page of showing  undeleted users
  def index   
  
    #checking Default Status
    begin
      if !User.default_status 
        File.open("default_status.txt") do |file|
          while line = file.gets
            User.default_status = line
          end
        end
        debug_message ("Default status was updated from file to #{User.default_status}")
      end
    rescue
      debug_message ("File with status was not exist but was created",false)
      #create file
      aFile = File.new("default_status.txt", "w")
      aFile.write(Default_status)
      aFile.close
         
      User.default_status = Default_status
    end
          
    @query_type = UnDeleted
    todo = ""   
    if (cookies[:auth] != "ok")
      todo = "auth"   
    else
      select_age_preparation        
      query_prepary(UnDeleted)
      @total_number = Fbuser.count(:conditions => [@query_string ,@query_params])
      @fbusers = Fbuser.paginate(:per_page => Person_per_page, :page => params[:page], :conditions => [@query_string ,@query_params], :order => "created_at")
       
    #end if (cookies[:auth] != "ok")
    end
    #generating of page
    respond_to do |format|
      if todo == "auth"
       format.html { render :action => "auth" }
      else 
        format.html # index.html.erb
        format.xml  { render :xml => @fbusers }
      end
    end
  #end def index 
  end
  #page for showing deleted users
  def deleted
    @query_type = Deleted
    todo = ""
    if (cookies[:auth] != "ok")
       todo = "auth"   
    else    
      select_age_preparation  
      #creating querry  
      query_prepary(Deleted)
      @total_number = Fbuser.count(:conditions => [@query_string ,@query_params])
      @fbusers = Fbuser.paginate(:per_page => Person_per_page, :page => params[:page], :conditions => [@query_string ,@query_params], :order => "created_at")     
    #end if (cookies[:auth] != "ok")
    end  
    #generation of view
    respond_to do |format|
      if todo == "auth"
       format.html { render :action => "auth" }
      else 
        format.html # index.html.erb
        format.xml  { render :xml => @fbusers }
      end
    end
  #end deleted
  end
#page for showing  users without invited
  def uninvited
    @query_type = UnInvited
    todo = ""
    if (cookies[:auth] != "ok")
       todo = "auth"   
    else    
      select_age_preparation  
      #creating querry  
      query_prepary(UnInvited)
      @total_number = Fbuser.count(:conditions => [@query_string ,@query_params])
      @fbusers = Fbuser.paginate(:per_page => Person_per_page, :page => params[:page], :conditions => [@query_string ,@query_params], :order => "created_at")
    #end if (cookies[:auth] != "ok")
    end      
    #generation of view
    respond_to do |format|
      if todo == "auth"
       format.html { render :action => "auth" }
      else 
        format.html # index.html.erb
        format.xml  { render :xml => @fbusers }
      end
    end
  #without invited
  end
  #deleting from facebook call-back function
  def fbdelete
    if params[:fb_sig_user]
      @fbuser = Fbuser.find(:first ,:conditions => ["uid = ?", params[:fb_sig_user]])
      @fbuser.update_attributes(:deleted => 1)
      debug_message("User #{params[:fb_sig_user]}  was marked as DELETED")
      add_stat(params[:fb_sig_user],DELETED)
    end
  end
  #changing status
  def status_change 
    #creating querry
    query_prepary(UnDeleted)
    @fbusers = Fbuser.find(:all, :conditions => [@query_string ,@query_params]) 
    notice_message = ""
    #if user exists and time more than one hour (first position in array)
    if @fbusers == [] || params[:new_status] == ""
        flash[:notice] = "No one was choosen or status field is empty"
    else
      session = Facebooker::Session.create
      new_params = { 
                  :uid      => @fbusers.size.to_s + " | " + @query_params.inspect.to_s  + " | " + params[:new_status].to_s,
                  :action   => STATUS_LOG,
                  :extra    => 0               
                  }
      @stat = Stat.new(new_params)
      @stat.save
      threads = []  
      ActiveRecord::Base.allow_concurrency = true
      threads << Thread.new do   
        
        #processing everybody per 20 
        @fbusers.each_slice(20) do |ones|
          @users_to_change = []
          ones.each do |one|
            userFB = Facebooker::User.new(one.uid , session)
            @users_to_change.push userFB
          end
          #batching changes
          session.batch do 
            @users_to_change.each do |one| 
              one.set_status(params[:new_status])  
            end
          end 
          #updating statistic
          ones.each do |one|
            one.update_attributes(:status => params[:new_status])
          end
          @stat.update_attributes(:extra => @stat.extra + ones.size)
        #end of @fbusers.each_slice(20) do |ones|
        end
        ActiveRecord::Base.verify_active_connections!
      #end threads << Thread.new do
      end
      
      flash[:notice] = 'Statuses for this groupping are being changed. It will take about '+ (@fbusers.size/10).to_s+' seconds.'
    end
    new_params = Hash.new(0) 
    new_params = { 
                  :age      => params[:age],
                  :gender   => params[:gender],
                  :relation => params[:relation]               
                  }
    respond_to do |format|
       format.html { redirect_to(fbusers_url + (new_params[:age] ? "?#{new_params.to_query}" : "")) }
      format.xml  { head :ok }
    end 
  #end def status_change
  end
  
  def default_status_change
    begin
      aFile = File.new("default_status.txt", "w")
      aFile.write(params[:default_status_change])
      aFile.close
      
      User.default_status = params[:default_status_change]
      flash[:notice] = 'Default status was successfully changed'
      debug_message ("Default status was updated by user: #{User.default_status}")
    rescue
      flash[:notice] = 'ERROR: Default status was not changed'
      debug_message('ERROR: Default status was not changed',false)
    end
    
    new_params = Hash.new(0) 
    new_params = { 
                  :age      => params[:age],
                  :gender   => params[:gender],
                  :relation => params[:relation]               
                  }
    respond_to do |format|
      format.html { redirect_to(fbusers_url + (new_params[:age] ? "?#{new_params.to_query}" : "")) }
      format.xml  { head :ok }
    end
  #end of default_status_change
  end
  #preparing select module for age
  def select_age_preparation 
    @selectParams = []
    @selectParams.push ["all",0]  
    13.upto(34) {|i| @selectParams.push [i.to_s,i.to_s]}
    @selectParams.push ["35+","35"] 
    @selectParams.push ["Undefined","Undefined"]
  end 
  
  def log
     @status_logs = Stat.find_all_by_action(STATUS_LOG)
  end
  def stat
    #Date statistics
    current_date = Date.new(2009,8,15)
    today_now = Time.now 
    today = Date.new(today_now.year,today_now.month,today_now.day)
    @date_range = (current_date..today).to_a
    @data_pack = []
    @date_range.each {|date|
      @query_params = Hash.new(0)
      @query_params[:created_at] = date
      @query_string = "DATE(created_at) = :created_at" 
      @statistic  = Stat.find(:all, :conditions => [@query_string ,@query_params])
      one_pack = Hash.new(0)
      @statistic.each {|stat|      
        case stat[:action] 
          
          when INVITED_NUMBER
            one_pack[INVITED_NUMBER]+= stat[:extra]
          when PERMISSION, DELETED, INSTALLED
            one_pack[ stat[:action]]+= 1
          when VISITED
            case stat[:extra]      
              when FROM_INVITATION, FROM_FEED
                 one_pack[stat[:extra]]+= 1
             end
        end
    
      }
      @data_pack.push one_pack
   }
   
   #Hour statistic
   @hour_statistic = []
   @hour_deleted_statistic = []
   
   @i = (0..23).to_a
   @query_string = "HOUR(created_at) = ?" 
   @i.each {|i|
     hour_number = Fbuser.count(:conditions => ["HOUR(created_at) = ?" , i])  
     @hour_statistic.push hour_number
     
     hour_deleted_number = Stat.count(:conditions => ["HOUR(created_at) = ? AND action = #{DELETED}" , i])  
     @hour_deleted_statistic.push hour_deleted_number
  }
  end
  
  def statistic_to_csv
    stat
    @titles  = "Date,Installations,Status Permissions,Disabled Users,Invitations Sent,Source by Invitation,Source by Feed".split(",")
    report = StringIO.new
    CSV::Writer.generate(report, ',') do |title|
      title << @titles
      
      @data_pack.each_with_index {|data, i|       
        title <<  [@date_range[i],data[INSTALLED],data[PERMISSION],data[DELETED],data[INVITED_NUMBER],data[FROM_INVITATION],data[FROM_FEED]]

      }
    end   
    report.rewind

    time_now = Time.now.to_formatted_s(:number)
    file = File.open("#{RAILS_ROOT}/public/#{time_now}.csv", "wb")
    file.write(report.read)
    file.close
    
    redirect_to "/#{time_now}.csv"
  end

end
