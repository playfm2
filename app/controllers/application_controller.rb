class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time
  #protect_from_forgery # See ActionController::RequestForgeryProtection for details
  filter_parameter_logging :password
  helper_method :facebook_session
  def set_current_user  
    if facebook_session && facebook_session.secured?
      if session[:user_id]
        @current_user ||= User.find(session[:user_id]) rescue nil
        @current_user = nil if @current_user && @current_user.facebook_id != facebook_session.user.id
      end
      if @current_user.nil? 
        @current_user = User.find_by_facebook_id(facebook_session.user.id) || User.create!(:facebook_id => facebook_session.user.id, :facebook_session_key => facebook_session.session_key)
        session[:user_id] = @current_user.id if @current_user
      end
      if @current_user && facebook_session.session_key != @current_user.facebook_session_key
        @current_user.update_attribute(:facebook_session_key, e.session_key)
      end
    else
      session[:user_id] = nil
      @current_user = nil
      clear_facebook_session_information
    end
  end
  def debug_message (message,is_not_error = true)
    if Debug_message_on
      unless is_not_error
        p "<>" * 25
        p "DEBUG ERROR:         "+ message
        p "<>" * 25
        MY_LOGGER.info(Time.now.to_s + ": "+message)
      else
        p " > > > > >" * 5
        p "          " + message
        p " > > > > >" * 5
      end   
    end
  end 
  def add_stat(uid, action, extra = 0)
     myParams = {
                :uid               => uid,
                :action            => action,
                :extra             => extra,
                }
      @stat = Stat.new(myParams)
      @stat.save  
      case action
        when INSTALLED
          debug_message ("STAT UPDATED: #{uid} installed application")
        when PERMISSION 
          debug_message ("STAT UPDATED: #{uid} gave permission")
        when INVITED_ENOUGH
          debug_message ("STAT UPDATED: #{uid} invited enough friends to participate")
        when DELETED
          debug_message ("STAT UPDATED: #{uid} deleted application")
        when INVITED_NUMBER
          debug_message ("STAT UPDATED: #{uid} invited #{extra} friends")
        when VISITED
          case extra
            when FROM_DIRECT
              debug_message ("STAT UPDATED: #{uid} came by direct link")
            when FROM_INVITATION
              debug_message ("STAT UPDATED: #{uid} came trough invitation")
            when FROM_FEED
              debug_message ("STAT UPDATED: #{uid} came from feeds")
          end
      end
      
  end 
  private :set_current_user
end
