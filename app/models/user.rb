class User < ActiveRecord::Base

  class << self
    attr_accessor :sessionkey, :sessionkey_time, :notifications_limit, :notifications_number, :default_status
  end

  @notifications_limit  = Hash.new(0)
  @notifications_number = Hash.new(0)

  def facebook_session
    @facebook_session ||= returning Facebooker::Session.create do |session|
      session.secure_with!(session_key,facebook_id,1.day.from_now)
    end
  end

end
