class Fbuser < ActiveRecord::Base
  def convert_birthday_to_number(db_birthday)
     now = DataTime.now
     mask = /(\d+)\-(\d+)\-(\d+)/
     birthday_arr = mask.match(db_birthday)
     birthday = DataTime.new(birthday_arr[1].to_i, birthday_arr[2].to_i, birthday_arr[3].to_i)
     return ((now - birthday)/365).to_i
  end

end
