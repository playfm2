module FbusersHelper
  def convert_birthday_to_number (db_birthday)
    ((DateTime.now - db_birthday)/365).to_i
  end
  
  def transform_hour (hour)
    if hour > 20 
      hour = hour - 21
    else
      hour = hour + 3
    end
    
    case hour    
      when 0
        return "midnight - 1AM"
      when 1..11  
        return "#{hour}AM - #{hour+1}AM"
      when 12
        return "12AM - 1PM"
      when 13..22
        return "#{hour%12}PM - #{hour%12+1}PM"
      when 23    
         return "11PM - midnight" 
    end
  end
end
