require 'test_helper'

class FbusersControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fbusers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fbuser" do
    assert_difference('Fbuser.count') do
      post :create, :fbuser => { }
    end

    assert_redirected_to fbuser_path(assigns(:fbuser))
  end

  test "should show fbuser" do
    get :show, :id => fbusers(:one).to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => fbusers(:one).to_param
    assert_response :success
  end

  test "should update fbuser" do
    put :update, :id => fbusers(:one).to_param, :fbuser => { }
    assert_redirected_to fbuser_path(assigns(:fbuser))
  end

  test "should destroy fbuser" do
    assert_difference('Fbuser.count', -1) do
      delete :destroy, :id => fbusers(:one).to_param
    end

    assert_redirected_to fbusers_path
  end
end
